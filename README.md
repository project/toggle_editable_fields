CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Toggle Editable Fields module is a formatter to transform "classic" boolean
field formatter to toggle editable field directly on 'view' display or on views
lists.

 * For a full description of the module visit:
   https://www.drupal.org/project/toggle_editable_fields

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/toggle_editable_fields


REQUIREMENTS
------------

This module requires the following library:

 * Bootstrap Toggle Plugin - https://github.com/minhur/bootstrap-toggle/


INSTALLATION
------------

Install the **Toggle Editable Fields** and **Libraries** modules as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

Update the repositories section of your composer file as follows:

```
    "repositories": {
        "drupal": {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        "minhur/bootstrap-toggle": {
            "type": "package",
            "package": {
              "name": "minhur/bootstrap-toggle",
              "version": "1.0.0",
              "type": "drupal-library",
              "dist": {
                "url": "https://github.com/minhur/bootstrap-toggle/archive/master.zip",
                "type": "zip"
              }
            }
        }
    },
```

Download the library by running `composer require minhur/bootstrap-toggle`

CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Create a boolean field.
    3. On display choose `Toggle Editable Formatter` as formatter and configure
       the settings.
    4. The user can now switch the state of the field in every place
       (view/entity view etc...).


MAINTAINERS
-----------

 * Alexandre Mallet (woprrr) - https://www.drupal.org/u/woprrr

Supporting organization:

 * NeoLynk - https://www.drupal.org/neolynk