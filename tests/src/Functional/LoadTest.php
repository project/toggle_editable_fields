<?php

namespace Drupal\toggle_editable_fields\Tests;

use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that the main page loads with the module enabled.
 *
 * @group toggle_editable_fields
 */
class LoadTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'field',
    'field_ui',
    'libraries',
  ];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Add debugging statement to check if the container is initialized.
    if (!$this->container) {
      throw new \Exception('Service container is not initialized');
    }

    // Create and log in a user with administer site configuration permission.
    $this->user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testLoad(): void {
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
  }

}
