<?php

namespace Drupal\toggle_editable_fields\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\field\FieldConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Build a form to switch state of targeted FieldItem.
 */
class AjaxToggleForm extends FormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new AjaxToggleForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user account proxy.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The FieldItem being targeted by this form.
   *
   * @var \Drupal\Core\Field\FieldItemInterface
   */
  protected $fieldItem;

  /**
   * The FieldDefinition being targeted by this form.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $fieldDefinition;

  /**
   * The current FieldItem name.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * The current FieldItem delta.
   *
   * @var int
   */
  protected $delta;

  /**
   * Default value of current FieldItem.
   *
   * @var bool
   */
  protected $defaultValue;

  /**
   * The field item plugin settings.
   *
   * @var array
   */
  protected $fieldSettings;

  /**
   * Initialize this Form Builder with FieldItem definition.
   *
   * Drupal only supports one form with a given ID per page,
   * so we generate a fieldItem specific ID at getFormId().
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   FieldItem to be displayed.
   * @param array $settings
   *   The formatter settings.
   */
  public function setFieldItem(FieldItemInterface $item, array $settings = []) {
    $this->fieldItem = $item;
    $this->fieldDefinition = $item->getFieldDefinition();
    $this->entity = $this->fieldItem->getEntity();
    $this->fieldName = $this->fieldDefinition->getName();
    $this->delta = $this->fieldItem->getName();
    $this->defaultValue = $this->fieldItem->value;
    $this->fieldSettings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    $parts = [
      'editable_ajax_toggle',
      $this->entity->getEntityTypeId(),
      $this->entity->id(),
      $this->fieldName,
      $this->delta,
      'form',
    ];
    return implode('_', $parts);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;
    $form['checkbox'] = [
      '#type' => 'checkbox',
      '#default_value' => $this->defaultValue,
      '#attributes' => [
        'data-toggle' => 'toggle',
        'class' => ['checkbox-toggle'],
      ],
      '#ajax' => [
        'callback' => [$this, 'formListAjax'],
        'event' => 'change',
        'progress' => [
          'type' => 'none',
        ],
      ],
      '#disabled' => !($this->fieldIsEditable() || $this->checkEditFieldAccess()),
    ];

    // Add library.
    $form['#attached']['library'][] = 'toggle_editable_fields/bootstrap.toggle';
    $this->setBootstrapDataAttributes($form['checkbox']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Update the clicked field with given value.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The current AjaxResponse.
   *
   * @throws \Exception
   *   Thrown when the entity can't found the clicked field name.
   */
  public function formListAjax(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    if (!empty($element)) {
      $this->updateFieldValue($form_state->getValue($element['#parents']));
    }

    return new AjaxResponse();
  }

  /**
   * Update the clicked field with given value.
   *
   * @param bool $value
   *   Value given by user.
   *
   * @throws \Exception
   *   Thrown when the entity can't found the clicked field name.
   */
  public function updateFieldValue($value) {
    if (!$this->entity->hasField($this->fieldName)) {
      throw new \Exception("No field $this->fieldName found in {$this->entity->id()} entity.");
    }

    if ($this->fieldIsEditable() || $this->checkEditFieldAccess()) {
      $this->entity->get($this->fieldName)->set($this->delta, $value);
      $this->entity->save();
    }
  }

  /**
   * Sets bootstrap data attributes for a given element.
   *
   * This method iterates over field settings and adds corresponding data
   * attributes to the provided form element if they are not already set.
   *
   * @param array $element
   *   An associative array representing the checkbox element of the form
   *   structure.
   */
  public function setBootstrapDataAttributes(array &$element) {
    foreach ($this->fieldSettings as $data_id => $data_value) {
      if ($data_value != NULL && !isset($element['#attributes']["data-$data_id"])) {
        $element['#attributes']["data-$data_id"] = $data_value;
      }
    }
  }

  /**
   * Check if the combo of entity + field access are satisfied.
   *
   * @return bool
   *   True if current user can edit the entity AND access current field.
   */
  public function fieldIsEditable() {
    return $this->entity->access('update') && $this->checkEditFieldAccess();
  }

  /**
   * Check only the access of the 'edit' operation for current field.
   *
   * @return bool
   *   True if current user can edit that field or FALSE.
   */
  protected function checkEditFieldAccess() {
    $permission_type = $this->fieldDefinition instanceof FieldConfigInterface ?
      $this->fieldDefinition->getFieldStorageDefinition()->getThirdPartySetting('field_permissions', 'permission_type') : NULL;

    $field_access = $this->entityTypeManager
      ->getAccessControlHandler($this->entity->getEntityTypeId())
      ->fieldAccess('edit', $this->fieldDefinition, $this->currentUser, $this->fieldItem->getParent());

    if ($field_access && empty($permission_type)) {
      return $this->entity->access('update');
    }

    return $field_access;
  }

}
