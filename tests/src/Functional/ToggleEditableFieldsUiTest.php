<?php

namespace Drupal\toggle_editable_fields\Tests;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;
use Drupal\toggle_editable_fields\Plugin\Field\FieldFormatter\ToggleEditableFormatter;

/**
 * Tests the Form mode manager user interfaces.
 *
 * @group toggle_editable_fields
 */
class ToggleEditableFieldsUiTest extends BrowserTestBase {

  use FieldUiTestTrait;

  /**
   * Common modules to install for this test.
   *
   * @var string[]
   */
  protected static $modules = [
    'block',
    'entity_test',
    'field',
    'field_ui',
    'libraries',
    'node',
    'taxonomy',
    'toggle_editable_fields',
    'user',
  ];

  /**
   * The default theme used for the test.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Stores the node content used by this test.
   *
   * @var \Drupal\node\Entity\Node[]
   */
  protected array $nodes = [];

  /**
   * Node entity type to test.
   *
   * @var \Drupal\node\Entity\NodeType
   */
  protected $nodeType1;

  /**
   * A user that can edit content types.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * Contains all data about the created field for this test.
   *
   * @var array
   */
  protected array $field = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Add a content type.
    $this->nodeType1 = $this->drupalCreateContentType();

    $this->adminUser = $this->drupalCreateUser([
      'administer content types',
      'administer node fields',
      'administer node display',
    ]);

    $this->drupalLogin($this->adminUser);
    $this->drupalPlaceBlock('system_breadcrumb_block');

    // Add a boolean field to the newly created content type.
    $label = $this->randomMachineName();
    $field_name = strtolower($label);
    $this->createBooleanField(
      $label,
      $field_name,
      $this->nodeType1->id(),
      'toggle_editable_formatter',
      [],
      [],
      ['label' => 'hidden', 'region' => 'content']
    );

    // Update the display settings using the new form submission methods.
    $session = $this->getSession();

    if (!$session) {
      throw new \Exception('Session not initialized.');
    }

    $page = $session->getPage();
    $assert_session = $this->assertSession();

    $this->drupalGet("admin/structure/types/manage/{$this->nodeType1->id()}/display");
    $edit = ["display_modes_custom[full]" => TRUE];

    // Fill out the form and submit it.
    foreach ($edit as $name => $value) {
      $page->fillField($name, $value);
    }
    $page->pressButton('Save');
    $assert_session->statusCodeEquals(200);

    // Generate content for this test.
    for ($i = 0; $i < 50; $i++) {
      $this->nodes[] = $this->createNode(['type' => $this->nodeType1->id()]);
    }
  }

  /**
   * Tests the boolean formatter field UI.
   */
  public function testFieldUi(): void {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();
    $default_settings = [
      'on' => 'On',
      'off' => 'Off',
      'size' => 'small',
      'onstyle' => 'success',
      'offstyle' => 'default',
    ];

    // Ensure the service container is initialized.
    if (!$this->container) {
      throw new \Exception('Service container is not initialized');
    }

    $this->drupalLogin($this->rootUser);
    $this->drupalGet("admin/structure/types/manage/{$this->nodeType1->id()}/display");
    $assert_session->statusCodeEquals(200);
    $this->assertDefaultFieldSettings();

    $this->drupalGet("admin/structure/types/manage/{$this->nodeType1->id()}/display/full");
    $assert_session->statusCodeEquals(200);
    $this->assertDefaultFieldSettings();

    $this->drupalGet("node/{$this->nodes[1]->id()}/edit");
    $page->fillField("{$this->field['name']}[value]", 1);
    $page->pressButton('Save');
    $assert_session->statusCodeEquals(200);

    // Add debugging statement to check if toggle_checkbox is found.
    $toggle_checkbox = $assert_session->elementExists(
      'xpath',
      '//input[contains(@data-toggle, "toggle")]'
    );

    if (!$toggle_checkbox) {
      throw new \Exception('Toggle checkbox not found');
    }

    foreach (array_keys($default_settings) as $key) {
      // Add debugging statement to check each data attribute.
      if (!$toggle_checkbox->hasAttribute("data-$key")) {
        throw new \Exception(new FormattableMarkup('Default data attribute %key not found.', ['%key' => "data-$key"]));
      }

      $this->assertTrue(
        $toggle_checkbox->hasAttribute("data-$key"),
        new FormattableMarkup('Default data attribute %key found.', ['%key' => "data-$key"])
      );
    }
  }

  /**
   * Asserts that default field settings are correctly set.
   */
  public function assertDefaultFieldSettings(): void {
    foreach (ToggleEditableFormatter::defaultSettings() as $value) {
      $this->assertSession()->pageTextContains($value);
    }
  }

  /**
   * Creates a new boolean field configured to use the custom formatter.
   *
   * @param string $label
   *   The label of the new field.
   * @param string $name
   *   The name of the new field (all lowercase), excluding the "field_" prefix.
   * @param string $type_name
   *   The node type that this field will be added to.
   * @param string $widget_name
   *   The name of the widget.
   * @param array $storage_settings
   *   Storage settings to override defaults.
   * @param array $field_settings
   *   Field instance settings to override defaults.
   * @param array $widget_settings
   *   Widget settings to override defaults.
   */
  protected function createBooleanField(string $label, string $name, string $type_name, string $widget_name, array $storage_settings = [], array $field_settings = [], array $widget_settings = []): void {
    $type_path = 'admin/structure/types/manage/' . $type_name;
    $this->fieldUIAddNewField($type_path, $name, $label, 'boolean', $storage_settings, $field_settings);

    $this->field = ['name' => "field_$name", 'label' => $label];
    $widget_settings += ['type' => $widget_name];

    $storage = $this->container->get('entity_type.manager')->getStorage('entity_view_display');
    if (!$storage) {
      throw new \Exception('Entity view display storage service not initialized.');
    }

    $view_display = $storage->load('node.' . $type_name . '.default');
    if (!$view_display) {
      throw new \Exception('View display not found for node type: ' . $type_name);
    }

    $view_display->setComponent($this->field['name'], $widget_settings)->save();
  }

}
